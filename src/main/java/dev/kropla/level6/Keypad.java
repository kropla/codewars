package dev.kropla.level6;

import java.util.Arrays;

public class Keypad {
    static String[] keyArray = {"1","abc2","def3","ghi4","jkl5","mno6","pqrs7","tuv8","wxyz9","*"," 0","#"};

    private static int searchPressAmountForKey(Character ch){
        return Arrays.stream(keyArray)
                .map(list -> list.indexOf(Character.toLowerCase(ch))+1)
                .filter(i -> i > 0)
                .findFirst().orElse(0);
    }

    public static int presses(String phrase) {
        return phrase.chars()
                .mapToObj(i -> (char)i)
                .map(ch -> searchPressAmountForKey(ch))
                .reduce(0, (a,b) -> a+b);
    }
}
