package dev.kropla.level6;

class Persist {

    public static int persistence(long n) {
        long resultSumOfDigits = n;
        int resultTimes = 0;
        while (resultSumOfDigits > 9) {
            resultTimes++;
            resultSumOfDigits = addDigits(resultSumOfDigits);
        }
        return resultTimes;
    }

    private static int addDigits(long n) {
        return Long.toString(n).chars()
                .mapToObj(i -> (char) i)
                .mapToInt(Character::getNumericValue)
                .reduce((a, b) -> a * b).getAsInt();
    }
}
