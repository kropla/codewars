package dev.kropla.level7;

import java.util.Arrays;
import java.util.Comparator;

public class MostDigits {
    public static int findLongest(int[] numbers) {
        return Arrays.stream(numbers).boxed()
                .max(Comparator.comparing(a -> Integer.toString(Math.abs(a)).length()))
                .get();
    }
}
