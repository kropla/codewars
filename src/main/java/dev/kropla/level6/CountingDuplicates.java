package dev.kropla.level6;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CountingDuplicates {
    public static int duplicateCount(String text) {
        Map<Integer, Integer> resultMap = new HashMap<>();
        text.toLowerCase().chars().forEach(character -> {
            Integer access = resultMap.getOrDefault(character, 0);
            resultMap.put(character, access + 1);
        });

        return (int) resultMap.entrySet().stream().filter(map -> map.getValue() > 1).count();
    }
}
