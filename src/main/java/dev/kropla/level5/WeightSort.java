package dev.kropla.level5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

public class WeightSort {

    public static String orderWeight(String strng) {
        return Arrays.stream(strng.split(" "))
                .sorted(bySumOfDigits.thenComparing(String::compareTo))
                .collect(Collectors.joining(" "));
    }

    static Comparator<String> bySumOfDigits = Comparator.comparing(
            str -> str.chars().map(ch -> ch-'0').sum()
    );
}
