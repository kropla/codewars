package dev.kropla.level6;

import java.util.Arrays;

public class FoldAnArray {

    static int[] workArray;

    public static int[] foldArray(int[] array, int runs) {
        if (runs > 0) {
            workArray = countFouldedArray(array);
            foldArray(workArray, --runs);
        }
        return workArray;
    }

    private static int[] countFouldedArray(int[] wArray) {
        int[] resultArray = Arrays.copyOf(wArray, (wArray.length / 2 + wArray.length % 2));
        for (int y = 0, z = wArray.length - 1; y < wArray.length / 2; y++, z--) {
            resultArray[y] = wArray[y] + wArray[z];
        }
        return resultArray;
    }
}