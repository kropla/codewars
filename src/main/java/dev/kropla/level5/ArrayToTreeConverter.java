package dev.kropla.level5;

public class ArrayToTreeConverter {
    static TreeNode tree;

    static TreeNode arrayToTree(int[] array) {
        tree = null;
        preOrderCreate(tree, array, 0);
        if (tree == null && array.length > 0) {
            tree = new TreeNode(array[0]);
        }
        return tree; // TODO: implementation
    }

    static void preOrderCreate(TreeNode node, int[] tab, int start) {
        int left = 2 * start + 1;
        int right = 2 * start + 2;

        if (left > tab.length || right > tab.length) return;

        if (node == null) {
            tree = new TreeNode(tab[start]);
            node = tree;
        }

        if (node.left == null && node.right == null) {
            if (left < tab.length) {
                node.left = new TreeNode(tab[left]);
            }
            if (right < tab.length) {
                node.right = new TreeNode(tab[right]);
            }
        }

        preOrderCreate(node.left, tab, left);
        preOrderCreate(node.right, tab, right);
    }
}

//generated class from cata author:
class TreeNode {

    TreeNode left;
    TreeNode right;
    int value;

    TreeNode(int value, TreeNode left, TreeNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    TreeNode(int value) {
        this(value, null, null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreeNode treeNode = (TreeNode) o;

        if (value != treeNode.value) return false;
        if (left != null ? !left.equals(treeNode.left) : treeNode.left != null) return false;
        return right != null ? right.equals(treeNode.right) : treeNode.right == null;
    }

    @Override
    public int hashCode() {
        int result = left != null ? left.hashCode() : 0;
        result = 31 * result + (right != null ? right.hashCode() : 0);
        result = 31 * result + value;
        return result;
    }
}
