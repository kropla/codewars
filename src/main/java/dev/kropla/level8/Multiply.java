package dev.kropla.level8;

/**
 * Created by Maciej_Kroplewski on 5/20/2017.
 */
public class Multiply {
    public static Double multiply(Double a, Double b) {
        return a * b;
    }
}