package dev.kropla.level6;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MultiplesOf3And5 {

    public int solution(int number) {
        Set<Integer> resultNumbers3 = IntStream.range(1, (number / 3) + 1).boxed().map(i -> i * 3).filter(num -> num < number).collect(Collectors.toSet());
        Set<Integer> resultNumbers5 = IntStream.range(1, (number / 5) + 1).boxed().map(i -> i * 5).filter(num -> num < number).collect(Collectors.toSet());

        resultNumbers3.addAll(resultNumbers5);

        return resultNumbers3.stream().mapToInt(Integer::intValue).sum();
    }
}