package dev.kropla.level7;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.Random;

public class MostDigitsTest {

    @Test
    public void exampleTest1() {
        assertEquals(100, MostDigits.findLongest(new int[]{1, 10, 100}));
    }

    @Test
    public void exampleTest2() {
        assertEquals(9000, MostDigits.findLongest(new int[]{9000, 8, 800}));
    }

    @Test
    public void exampleTest3() {
        assertEquals(900, MostDigits.findLongest(new int[]{8, 900, 500}));
    }

    @Test
    public void exampleTest4() {
        assertEquals(40000, MostDigits.findLongest(new int[]{3, 40000, 100}));
    }

    @Test
    public void exampleTest5() {
        assertEquals(100000, MostDigits.findLongest(new int[]{1, 200, 100000}));
    }

    @Test
    public void exampleTest6() {
        assertEquals(-10, MostDigits.findLongest(new int[]{-10, 1, 0, 1, 10}));
    }

    @Test
    public void randomTests() {
        Random random = new Random();

        for (int i = 0; i < 40; ++i) {
            int size = random.nextInt(50) + 1;
            int[] numbers = new int[size];

            int bestNumber = random.nextInt();
            int bestDigits = (int) Math.log10(Math.abs(bestNumber));

            numbers[0] = bestNumber;

            for (int n = 1; n < size; ++n) {
                int number = random.nextInt();
                int digits = (int) Math.log10(Math.abs(number));

                if (digits > bestDigits) {
                    bestNumber = number;
                    bestDigits = digits;
                }

                numbers[n] = number;
            }

            assertEquals(bestNumber, MostDigits.findLongest(numbers));
        }
    }
}