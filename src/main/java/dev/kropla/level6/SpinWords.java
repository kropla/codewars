package dev.kropla.level6;

import java.util.Arrays;

public class SpinWords {

    public String spinWords(String sentence) {
        String[] words = sentence.split(" ");
        StringBuilder result = new StringBuilder();
        Arrays.stream(words).forEach(word -> {
            if (word.length() > 4) {
                StringBuilder sb = new StringBuilder(word);
                word = sb.reverse().toString();
            }
            result.append(word + " ");
        });
        return result.deleteCharAt(result.length() - 1).toString();
    }
}