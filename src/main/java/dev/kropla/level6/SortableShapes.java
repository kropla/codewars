package dev.kropla.level6;

public class SortableShapes implements Comparable {
    double area, side, radius, base, height, width;

    @Override public int compareTo(Object o) {
        return Double.compare(this.area, ((SortableShapes) o).area);
    }
}

class CustomShape extends SortableShapes {
    CustomShape(double area) {
        this.area = area;
    }
}
class Square extends Rectangle {
    Square(double side) {
        super(side, side);
    }
}
class Rectangle extends CustomShape {
    Rectangle(double width, double height) {
        super(width * height);
    }
}
class Triangle extends CustomShape {
    Triangle(double base, double height) {
        super((base * height) / 2);
    }
}
class Circle extends CustomShape {
    Circle(double radius) {
        super( radius * radius * Math.PI);
    }
}