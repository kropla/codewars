package dev.kropla.level6;

public class BitCounting {

    public static int countBits(int n) {
        String value = Integer.toString(n, 2);
        return (int) value
                .chars()
                .filter(c -> c == '1')
                .count();
    }
}