package dev.kropla.level6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MultiplesOf3And5Test {
    @Test
    public void test() {
        assertEquals(23, new MultiplesOf3And5().solution(10));
        assertEquals(78, new MultiplesOf3And5().solution(20));
        assertEquals(9168, new MultiplesOf3And5().solution(200));
    }
}