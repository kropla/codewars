package dev.kropla.level6;

import java.util.stream.Collectors;

public class KillerGarageDoor {

    public static String run(String events) {
        DoorStateContext context = new DoorStateContext();
        return events.chars().map(i -> (char) i)
                .map(c -> interpretSignal(c, context))
                .mapToObj(b -> Integer.toString(b))
                .collect(Collectors.joining());
    }

    private static byte interpretSignal(int c, DoorStateContext context) {
        switch ((char) c) {
            case 'P':
                return context.buttonPressedEvent();
            case 'O':
                return context.obstacleDetected();
            default:
                return context.noEvent();
        }
    }
}

interface DoorState {
    default void noEvent(DoorStateContext context) {
    }

    void buttonPressedEvent(DoorStateContext context);

    default void obstacleDetected(DoorStateContext context) {
    }
}

class DoorClosedState implements DoorState {
    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorOpeningState());
        context.openOneStep();
    }
}

class DoorOpenedState implements DoorState {
    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorClosingState());
        context.closeOneStep();
    }
}

class DoorOpeningState implements DoorState {

    @Override
    public void noEvent(DoorStateContext context) {
        context.openOneStep();
        if (context.isDoorOpened()) {
            context.setState(new DoorOpenedState());
        }
    }

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorPausedState(this));
    }

    @Override
    public void obstacleDetected(DoorStateContext context) {
        context.closeOneStep();
        context.setState(new DoorClosingState());
    }

}

class DoorClosingState implements DoorState {

    @Override
    public void noEvent(DoorStateContext context) {
        context.closeOneStep();
        if (context.isDoorClosed()) {
            context.setState(new DoorClosedState());
        }
    }

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorPausedState(this));
    }

    @Override
    public void obstacleDetected(DoorStateContext context) {
        context.openOneStep();
        context.setState(new DoorOpeningState());
    }

}

class DoorPausedState implements DoorState {

    private DoorState doorState;

    public DoorPausedState(DoorState doorState) {
        this.doorState = doorState;
    }

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(doorState);
        context.noEvent();
    }

}

class DoorStateContext {
    private DoorState state;
    private byte doorStep = 0;

    public DoorStateContext() {
        this.state = new DoorClosedState();
    }

    public void setState(DoorState state) {
        this.state = state;
    }

    public byte noEvent() {
        state.noEvent(this);
        return doorStep;
    }

    public byte buttonPressedEvent() {
        state.buttonPressedEvent(this);
        return doorStep;
    }

    public byte obstacleDetected() {
        state.obstacleDetected(this);
        return doorStep;
    }

    public void openOneStep() {
        doorStep++;
    }

    public void closeOneStep() {
        doorStep--;
    }

    public boolean isDoorOpened() {
        return doorStep == 5;
    }

    public boolean isDoorClosed() {
        return doorStep == 0;
    }
}