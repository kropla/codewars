package dev.kropla.level6;

import org.junit.Test;

import java.util.*;

import static junit.framework.Assert.assertEquals;

public class SortableShapesTests {

    @Test
    public void ShapesAreSortableOnArea() {
        // Arrange
        double area, side, radius, base, height, width;
        ArrayList<SortableShapes> expected = new ArrayList<SortableShapes>();

        area = 1.1234;
        expected.add(new CustomShape(area));

        side = 1.1234;
        expected.add(new Square(side));

        radius = 1.1234;
        expected.add(new Circle(radius));

        height = 2.;
        base = 5.;
        expected.add(new Triangle(base, height));

        height = 3.;
        base = 4.;
        expected.add(new Triangle(base, height));

        width = 4.;
        expected.add(new Rectangle(width, height));

        area = 16.1;
        expected.add(new CustomShape(area));

        Random random = new Random();
        ArrayList<SortableShapes> actual = createRandomOrderedList(random, expected);

        // Act
        Collections.sort(actual);

        // Assert
        Iterator a = actual.iterator();
        for (SortableShapes e : expected) {
            assertEquals(e, a.next());
        }
    }

    @Test
    public void RandomizedTest() {
        // Arrange
        Random random = new Random();
        ArrayList<ShapeContainer> expected = new ArrayList<ShapeContainer>();
        List<SortableShapes> actual = new ArrayList<SortableShapes>();
        for (int i = 0; i < 25000; i++) {
            SortableShapes shape;
            double area;
            switch (random.nextInt(5)) {
                case 0:
                    double side = random.nextDouble();
                    shape = new Square(side);
                    area = side * side;
                    break;
                case 1:
                    double width = random.nextDouble();
                    double height = random.nextDouble();
                    shape = new Rectangle(width, height);
                    area = width * height;
                    break;
                case 2:
                    double base = random.nextDouble();
                    height = random.nextDouble();
                    shape = new Triangle(base, height);
                    area = (base * height) / 2;
                    break;
                case 3:
                    double radius = random.nextDouble();
                    shape = new Circle(radius);
                    area = (Math.pow(radius, 2) * Math.PI);
                    break;
                default:
                    area = random.nextDouble();
                    shape = new CustomShape(area);
                    break;
            }
            expected.add(new ShapeContainer(area, shape));
            actual.add(shape);
        }
        Collections.sort(expected, new ShapeComparator());

        // Act
        Collections.sort(actual);

        // Assert
        Iterator a = actual.iterator();
        for (ShapeContainer e : expected) {
            assertEquals(e.shape, a.next());
        }
    }

    private class ShapeContainer {
        public double area;
        public SortableShapes shape;

        public ShapeContainer(double area, SortableShapes shape) {
            this.area = area;
            this.shape = shape;
        }
    }

    private class ShapeComparator implements Comparator<ShapeContainer> {
        public int compare(ShapeContainer c1, ShapeContainer c2) {
            if (c1.area == c2.area) return 0;
            return c1.area > c2.area ? 1 : -1;
        }
    }

    private ArrayList<SortableShapes> createRandomOrderedList(Random random, ArrayList<SortableShapes> expected) {
        ArrayList<SortableShapes> actual = new ArrayList<SortableShapes>();
        for (SortableShapes shape : expected) {
            int j = random.nextInt(actual.size() + 1);
            actual.add(j, shape);
        }
        return actual;
    }
}