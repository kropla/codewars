package dev.kropla.level5;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringToCamelCaseConverter {


    static String toCamelCase(String s) {
        return Stream.concat(
                Arrays.stream(s.split("_|-"))
                        .limit(1)
                , Arrays.stream(s.split("_|-"))
                        .skip(1)
                        .map(str -> Character.toUpperCase(str.charAt(0)) + str.substring(1))
        ).collect(Collectors.joining());
    }
}
