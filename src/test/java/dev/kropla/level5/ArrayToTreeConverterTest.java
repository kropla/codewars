package dev.kropla.level5;

import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class ArrayToTreeConverterTest {

    @Test
    public void emptyArray() {
        TreeNode expected = null;
        assertThat(ArrayToTreeConverter.arrayToTree(arrayFrom()), is(expected));
    }

    @Test
    public void arrayWithSingleElement() {
        TreeNode expected = new TreeNode(7);
        assertThat(ArrayToTreeConverter.arrayToTree(arrayFrom(7)), is(expected));
    }

    @Test
    public void arrayWithMultipleElements() {
        TreeNode expected = new TreeNode(17, new TreeNode(0, new TreeNode(3), new TreeNode(15)), new TreeNode(-4));
        assertThat(ArrayToTreeConverter.arrayToTree(arrayFrom(17, 0, -4, 3, 15)), is(expected));
    }

    @Test
    public void randomSmallArray() {
        int[] values = new java.util.Random().ints(-1000, 1001).limit(13).toArray();
        assertThat(ArrayToTreeConverter.arrayToTree(values), is(arrayToTree(values)));
    }

    @Test
    public void randomBigArray() {
        int[] values = new java.util.Random().ints(-1000, 1001).limit(234).toArray();
        assertThat(ArrayToTreeConverter.arrayToTree(values), is(arrayToTree(values)));
    }

    private int[] arrayFrom(int... values) {
        return values;
    }

    private static TreeNode arrayToTree(int[] array) {
        if (array.length == 0) return null;

        Queue<TreeNode> queue = new LinkedList<>();
        TreeNode root = new TreeNode(array[0]);
        queue.add(root);
        for (int i = 0; !queue.isEmpty(); i++) {
            TreeNode node = queue.remove();
            node.left = nodeFromIndex(array, 2 * i + 1);
            if (node.left != null) queue.add(node.left);
            node.right = nodeFromIndex(array, 2 * i + 2);
            if (node.right != null) queue.add(node.right);
        }
        return root;
    }

    private static TreeNode nodeFromIndex(int[] array, int index) {
        if (array.length <= index) return null;
        else return new TreeNode(array[index]);
    }
}