package dev.kropla.level5;

public class StringMerger {

    public static boolean isMerge(String s, String part1, String part2) {

        StringBuilder sbs = new StringBuilder(s), sb1 = new StringBuilder(part1), sb2 = new StringBuilder(part2), theSameBuffer = new StringBuilder();
        boolean eraseTheSameBuf = false;

        while (sbs.length() > 0) {
            char sChar = sbs.charAt(0);
            sbs.deleteCharAt(0);

            if (sb2.length() > theSameBuffer.length()
                    && sb1.length() > 0
                    && sb1.charAt(0) == sb2.charAt(theSameBuffer.length())) {
                theSameBuffer.append(sb1.charAt(0));
                eraseTheSameBuf = false;
            } else {
                eraseTheSameBuf = true;
            }

            if (sb1.length() > 0 && sChar == sb1.charAt(0)) {
                sb1.deleteCharAt(0);
            } else if (sb2.length() > 0 && sChar == sb2.charAt(0)) {
                sb2.deleteCharAt(0);
            } else if (theSameBuffer.length() > 0
                    && sb2.length() > theSameBuffer.length()
                    && sb2.charAt(theSameBuffer.length()) == sChar) {
                StringBuilder temp = new StringBuilder(sb1);
                sb1 = new StringBuilder(sb2);
                sb2 = new StringBuilder(temp);
                sbs.insert(0, sChar);
                sbs.insert(0, theSameBuffer);
                sb2.insert(0, theSameBuffer);
                eraseTheSameBuf = true;
            } else {
                return false;
            }
            if (eraseTheSameBuf) {
                theSameBuffer.setLength(0);
            }
        }

        return (sb1.length() == 0 && sb2.length() == 0);
    }

}